\chapter{Conclusions and Future Work}

Writing bug-free code is a primary task for developers, and in order to help them to automatically identify bugs in their code base researchers have come up with different methods for finding bugs. Most of those solutions are based on static analysis of the code, where a bug would be detected when a piece of code follows some predefined rules. Those approaches are powerful but are not flexible, and they require to lay down rules for each specific type of bug of interest.

In this thesis we presented an alternative approach to this problem, where we tried to train a neural network to automatically detect if a line of code in a method is buggy or not.

To train such model we used data from open source Java projects on GitHub. Using the service of GHArchive we wrote a program that collected all the commits from 2015 to 2019 on all public Java repositories in which the commit message referred to some bug fix: e.g., ``Fixed the issue...'', ``The bug has been solved''. In specific, for each identified bug-fixing commit, we collected the code before the commit, and the code after the commit, based on the assumption that the code before would be ``buggy'', while the committed version would be ``fixed''.

We processed all the collected data, keeping only commits where a single line of code has been changed, this was done to help the model learn on simpler examples, since identifying a bug where many lines have been changed can be more challenging.

Before feeding the methods to our neural network, we performed some token transformation to reduce the vocabulary size: for each possible Java token type (i.e., methods name, types name, variables name, etc.) we kept only the tokens that would add up to 20\% of their respective category. The rest of tokens has been transformed into a ``generic form'': variable0, method1, type7, etc.

Our final dataset contained 34,256 buggy methods and 34,256 fixed ones. We designed our neural network model to have 3 inputs: the lines of code before the bug, the buggy line, the lines of code after the bug. Each of those inputs has then been embedded and fed into a RNN layer. All RNN layers have been concatenated and connected to a dense layer, finally connected to the output layer. Our model produces a value between 0 and 1, indicating the probability that a line of code in the method we fed as input is buggy (1) or not buggy (0).

We trained our model by splitting the dataset into 80\% training, 10\% validation and 10\% test. During the training we explored several hyperparameters combinations, based on the results we choose one that had 2 stacked layers of RNN.

We tested our model first ``as is'', obtaining poor results with around 55\% of accuracy. We then introduced the concept of threshold, to help the performance of the model at the cost of not being able to produce a result for some predictions. With the introduction of the confidence threshold we were able to reach up to 80\% accuracy, a result that shows the possibility of learning features of the characterizing buggy and clean code lines. Still, more research is needed to obtain something that can be used by developers.

Given our results, additional experiments could be done, such as:

\begin{itemize}
\item \emph{Improve the quality of the dataset}. By filtering some repositories it might be possible to improve the quality of the code present in the dataset, possibly helping the learning process.

\item \emph{Training on ``larger'' bug-fixes that may spread over multiple lines}. While our conjecture was that simpler bug-fixes only modifying a single line would help the learning, we did not verify such an assumption.

\item \emph{Experiment with different and more recent deep learning models.} For example, recently proposed Transformer models.

\item \emph{Experiment with different programming languages.} Simpler syntax may result in better performance.
\end{itemize}
