\chapter{Introduction}

Two of the main concerns for a developer are the quality and correctness of his code.
In the last years, researchers have developed several "code analysis" tools to help in the achievement of these two important goals. They propose automatic mechanisms to check the quality and the correctness of the written code, also suggesting improvements and corrections \cite{10.1145/581339.581377}\cite{1}\cite{10.1145/1287624.1287630}\cite{Livshits05dynamine:finding}. The main goal is to detect bad practices, such as, unnecessary code repetition or to detect known patterns that may result in a bug. Most of the proposed solutions are rule-based approaches, meaning that the code is analyzed looking for violations of some known rules. Each violation will be marked as a potential bug and then checked.

% Null check example
For example, let's take into consideration the common bug where you try to invoke a method on a variable that is \textit{null}, also known as a \textit{NullPointerException}. A code analysis tool can easily detect the pattern that causes this exception by tracking the usage of the corresponding variable \cite{nullpointer}; If it recognizes that the variable may be null when something is being invoked on it, it can suggest adding a null check before the method call
\begin{lstlisting}[language=Java,basicstyle=\small]
  // if user is null this will cause a NullPointerException
  user.getEmail()

  // the code analasys tool may suggest to fix it with a simple check
  if (user != null) {
    user.getEmail()
  }
\end{lstlisting}

Detecting bugs with these rule-based approaches can be very effective, however, it is limited by the number of \textit{rules} that are predefined.
Defining new rules takes a lot of time and research for each one of them. It would be easier to automatically learn the patterns that cause a bug.

In order to learn those patterns, we need a lot of data. Luckily, the open-source community and open source projects in generals are more active than ever. Thanks to this nowadays websites like GitHub host million of repositories, featuring billions of lines of code.

In this thesis we propose an approach that exploit the availability of this data and deep learning models to learn the characteristics of buggy and non-buggy code. This means that, once trained, the model will be able to detect potential bugs in the code (i.e., to identify buggy patterns) without the need to manually specify any detection rule. This characteristic makes our approach more general and it may be able to detect bugs that may not trigger any of the rule-based approaches.
Similar approaches have been already researched \cite{neuralbug}\cite{7582806}\cite{DBLP:journals/corr/abs-1805-11683}. However, our solution differs mostly because our dataset has been generated from real human-written code.

\section{Objectives and Results}
The aim of the project is to develop an alternative solution for bug detection. We propose an approach based on a deep learning model that is able to detect bugs that may not trigger any of the rule-based approaches.

In the first part of the project we focus on the development of the deep learning model, comparing our problem with similar ones, also with some that are not code-related.

After the choice of the most suitable deep-learning model, we focused on the choice of the training dataset. We decided to collect the bug samples directly from GitHub, so that the code that will be used to train the model is actual human-written code, and not generated one. This is done because artificially generated bugs may not reflect real-world bugs, resulting in lower performance of the model. Also, for each ``buggy code'', we also retrieve its fixed version, by mining bug-fixing commits in open source projects (i.e., code changes in which a bug has been fixed, providing us with the version before and after the fix). Such a dataset has been pre-processed before serving it to the neural network.

The final part of the project consists of the empirical study to test the effectiveness of our approach. We will see all the different hyperparameters that we tuned during the training of the network and we will see how their change impacts the performance of the model, and which ones perform the best in our case.

Our model is limited to Java code. To ensure the best performance of the model we limited it to a single programming language, however, the same approach could be adapted to any language, by simply changing the training dataset.
Other restrictions have been applied during the creation of the dataset, like the size of the methods affected by the bug, and the number of lines that could compose a bug. The last one in particular in our experiments was limited to bugs that affect a single line of code.
All those restriction were meant to give the model the easiest task possible, since detecting a bug on a single line should be easier than a bug spread over multiple lines

Thanks to the applied restrictions, in the specific analyzed cases, our approach shows good results. In fact, it reaches 80\% accuracy, using 0.95 level of confidence.

\section{Structure of the Thesis}

This document is organized as follows:
\begin{itemize}
  \item Chapter 2 presents the state of the art on automatic bug detection, exploring existing solutions and their results.
  \item Chapter 3 focuses on the solution proposed by this thesis for detecting bugs using a RNN. It starts presenting the approach used by our solution, comparing with the existing methods. Then, it presents the deep learning model used for the bug detection.
  \item In Chapter 4 we focus on the description of our empirical study and how it has been completed. Initially we present the dataset used for the neural network training, how it has been obtained and generated before feeding it into the RNN. We then are going to cover the structure of the RNN and explain the training process.
  \item Chapter 5 presents the achieved results.
  \item Chapter 6 presents the limitation of our approach, explaining some of the choices made during the creation of the dataset, such as the number of lines of the methods used.
  \item Chapter 7 provides some conclusions and outlook for future works.
\end{itemize}