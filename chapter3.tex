\chapter{Detecting Buggy Lines using RNN}
\section{Mining Buggy and Non-Buggy Lines}
In order to create a large enough dataset to train a deep learning model able to automatically identify bugs we decided to mine bugs on public GitHub repositories, in particular, Java ones.

Our goal was to extract buggy and non-buggy lines of code from a repository. To achieve this we looked into the commits of GitHub repositories, searching for the ones in which the commit message indicates the \textit{fix} of a bug. Given a bug-fixing commit, we can assume that the modified code before the bug-fix was buggy, while after it is clean (i.e., correct).

We then processed the collected code instances through a number of filters aimed at making the collected code instances suitable for the deep learning model. The final output of this process is represented by buggy and non-buggy lines of code.

\subsection{Collect Data From GitHub}

The first task to extract the code from the repositories is done by using \href{https://www.gharchive.org/}{GH Archive}. This site provides all the public GitHub events that happen in a specific hour of a day on public repositories. Using the API provided by GH Archive we wrote a \textit{Swift} Project that retrieves all the events from \textit{2015-01-01} to \textit{2019-11-15}.
Of all those events we are only interested in the \textit{push} events, but GH Archive also provides merge requests, issues and other types of event.
We proceeded to filter out the push events that do not contain both of these regular expressions in the commit message:

\begin{itemize}
  \item \texttt{bug|issue|problem}
  \item \texttt{fix|solve}
\end{itemize}

Basically we collected all commits in which developers wrote in the commit message sentences such as: ``\emph{Fixed the issue ...}'', ``\emph{The bug has been solved}'', ``\emph{Solve the ... problem}'' and so on.

As a next step we filtered out all commits not performed in a repository mostly containing Java Code. To know if Java is the main language of a repository we used the GitHub APIs that provide such information. This has been the biggest bottleneck of the process in term of time, since GitHub APIs are free but with a 5,000 request per hour limit. To avoid excessive slowdown the Swift program cycles between 13 different tokens (thus increasing the amount of possible requests to 65k per hour).

The output of this first step is a directory structured as follow:
\dirtree{%
  .1 data.
  .2 author@repository.
  .3 sha-commit.
  .4 before.
  .5 file1.java.
  .5 file2.java.
  .5 \dots.
  .4 after.
  .5 file1.java.
  .5 file2.java.
  .5 \dots.
  .2 author@repository.
  .3 sha-commit.
  .4 \dots .
}

We save each commit in a folder using the commit SHA as folder name. Each commit is stored in a folder representing the repository, to avoid name conflicts we add the author name at the front followed by a `@' character.
Each commit will then contain two folders, before and after, each one containing all the files modified by that commit.
In total we have collected 648,835 commits in 204,830 different repositories

\subsubsection{Preprocessing and Dataset Creation}
Given the previously described folder structure, we process each Java file to remove all the unneeded information, and generate a single file representing our dataset.

First, we wrote a second program in Java having the goal of implementing a code abstraction process. This helps in: (i) reducing the vocabulary size of the dataset, since different code tokens (e.g., different identifiers) will be just represented by the same abstract token indicating the AST type of the raw code token. To decide which token to keep and which to abstract we first extracted the AST type of each code token, thus collecting class fields, methods calls, function parameters, types names, and variables names. Then, we counted the occurrences of their value. Figure \ref{fig:methodscalls} shows the method calls distribution. As we can see the most common tokens will contain the majority of all the tokens for this category, and we observed that this pattern is present in all token types. In particular looking at the top 10 methods call we have the Table \ref{tab:methodscalls}.

% METHODS CALLS
% \begin{minipage}{0.65\textwidth}
\begin{figure}[H]
  \includegraphics[width=\linewidth]{word_occurences/methodCalls.png}
  \caption{Methods calls occurencies}
  \label{fig:methodscalls}
\end{figure}
% \end{minipage}
\begin{table}[H]
  \centering
  \begin{tabular}{|l|r|}
    \hline get       & 203,244 \\
    \hline add       & 142,579 \\
    \hline equals    & 110,863 \\
    \hline put       & 93,068  \\
    \hline append    & 85,112  \\
    \hline toString  & 78,452  \\
    \hline println   & 69,256  \\
    \hline size      & 63,824  \\
    \hline getString & 58,129  \\
    \hline getName   & 49,880  \\
    \hline
  \end{tabular}
  \caption{Top 10 methods calls}
  \label{tab:methodscalls}
\end{table}

We can see that most of these are calls from methods of standard Java classes. We decided to keep the tokens that would add up to $20\%$ of their respective occurrences count. This would help to keep the vocabulary for the dataset small, keeping tokens where the semantic can be easily learn through different examples, while removing all the domain specific one, that with very few example would be just noise in the learning process.

\begin{figure}[H]
  \includegraphics[width=\linewidth]{word_occurences/fields.png}
  \captionof{figure}{Fields name occurencies}
  \label{fig:fieldnames}
\end{figure}
\begin{table}[H]
  \centering
  \begin{tabular}{|l|r|}
    \hline out         & 58,831 \\
    \hline id          & 53,456 \\
    \hline length      & 36,979 \\
    \hline string      & 21,272 \\
    \hline swing       & 16,151 \\
    \hline GroupLayout & 12,450 \\
    \hline layout      & 10,091 \\
    \hline y           & 9,199  \\
    \hline x           & 9,128  \\
    \hline drawable    & 8,028  \\
    \hline
  \end{tabular}
  \caption{Top 10 fields names}
  \label{tab:fieldnames}
\end{table}

Looking at the distribution of the fields names in Figure \ref{fig:fieldnames} and Table \ref{tab:fieldnames} we can see a similar result, but this time with 2 values close to first, ``out'' probability for the java \texttt{System.out}, and ``id'' since it's widley used. In the remaining we can see both ``generic'' names and ones that are specific to Android development.

\begin{figure}[H]
  \includegraphics[width=\linewidth]{word_occurences/parameters.png}
  \caption{Parameter names occurencies}
  \label{fig:parameters}
\end{figure}
\begin{table}[H]
  \centering
  \begin{tabular}{|l|r|}
    \hline e                  & 84,095 \\
    \hline ex                 & 10,908 \\
    \hline v                  & 9,437  \\
    \hline args               & 9,297  \\
    \hline request            & 6,778  \\
    \hline view               & 6,495  \\
    \hline savedInstanceState & 6,458  \\
    \hline context            & 5,704  \\
    \hline event              & 5,629  \\
    \hline response           & 5,604  \\
    \hline
  \end{tabular}
  \caption{Top 10 parameter names}
  \label{tab:parameters}
\end{table}

The majority of the parameters are bound to a single value as we can see in Figure \ref{fig:parameters} and in Table \ref{tab:parameters}. Is common practice in Java calling the parameter of a try catch exception ``e'', so is pretty common to find it several times in a project. We can also note that the second most used value is ``ex'' also another identifier for the exceptions

\begin{figure}[H]
  \includegraphics[width=\linewidth]{word_occurences/types.png}
  \caption{Types occurencies}
  \label{fig:types}
\end{figure}
\begin{table}[H]
  \centering
  \begin{tabular}{|l|r|}
    \hline String    & 786,828 \\
    \hline int       & 222,901 \\
    \hline void      & 116,922 \\
    \hline List      & 75,435  \\
    \hline boolean   & 71,956  \\
    \hline ArrayList & 54,103  \\
    \hline Exception & 50,142  \\
    \hline Object    & 48,284  \\
    \hline Map       & 39,896  \\
    \hline File      & 35,593  \\
    \hline
  \end{tabular}
  \caption{Top 10 types}
  \label{tab:types}
\end{table}

The distribution of the types is shown in Figure \ref{fig:types} and Table \ref{tab:types}.
The most common type is ``String'', followed by ``int''. We can see that all most common types are either primitives of the language or from Java standard libraries.

\begin{figure}[H]
  \includegraphics[width=\linewidth]{word_occurences/variables.png}
  \caption{Variable names occurencies}
  \label{fig:variables}
\end{figure}
\begin{table}[H]
  \centering
  \begin{tabular}{|l|r|}
    \hline i       & 229,052 \\
    \hline R       & 97,281  \\
    \hline e       & 88,320  \\
    \hline System  & 87,608  \\
    \hline result  & 56,208  \\
    \hline request & 50,176  \\
    \hline j       & 39,340  \\
    \hline context & 39,030  \\
    \hline value   & 33,717  \\
    \hline c       & 32,662  \\
    \hline
  \end{tabular}
  \caption{Top 10 variable names}
  \label{tab:variables}
\end{table}

Lastly the distribution for variable names is shown in Figure \ref{fig:variables} and Table \ref{tab:variables}. Here we can see that the most used is ``i'', generally used as a index in loops, followed by ``R'' again an Android specific token, that usually indicate a resource.

The tokens that do not fall in the $20\%$ that we selected have been transformed into a generic form, i.e. \texttt{methodCall0}.

We can take this method as an example: [Listing \ref{lst:example_method}]\\
\begin{minipage}{\linewidth}
  \begin{lstlisting}[language=Java,frame=single, caption=Example method, label=lst:example_method]
static void myMethod(String[] args) {
  Point myPoint = new Point();
  int x = myPoint.getX();
  int y = myPoint.getY();
  for (int i = 0; i < y; i++) {
    System.out.println(x);
    System.out.println(i + y);
  }
}
\end{lstlisting}
\end{minipage}

If we replace all the tokens according to our rules we would get:\\
\begin{minipage}{\linewidth}
  \begin{lstlisting}[language=Java,frame=single, caption=Example method after code transformation
, label=lst:modified_example_method]
static void myMethod(String[] args) {
  Type0 variable0 = new Type0();
  int x = variable0.methodCall0();
  int y = variable0.methodCall1();
  for (int i = 0; i < y; i++) {
    System.out.println(x);
    System.out.println(i + y);
  }
}
\end{lstlisting}
\end{minipage}

As you can see the most common tokens like "i" or "System" or "String"  stayed there, while the less frequent ones are now "generic terms" but they still maintain their logic usage.

Even though we feed a whole method to the classifier the granularity of the bug detection is "for line" instead then "for method":  this is because for each commit we computed the git diff, and considered only bug-fixing commits that have a delta of 1 line, we then marked that line in the final dataset as buggy for the version of the code before the bug-fixing commit and as clean for the version of the commit after the bug-fixing.

Let's assume the for loop of the previous example was buggy, the result with the marked line would be:\\
\begin{minipage}{\linewidth}
  \begin{lstlisting}[language=Java,frame=single, caption=Example method after code transformation and marked bugged line
, label=lst:modified_example_method]
static void method(String[] args) {
  Type0 variable0 = new Type0();
  int x = variable0.methodCall0();
  int y = variable0.methodCall1();
#  for (int i = 0; i < y; i++) {
    System.out.println(x);
    System.out.println(i + y);
  }
}
\end{lstlisting}
\end{minipage}

After this transformation we restricted the dataset to methods under 80 lines in order to feed the neural network simpler data.

\subsubsection{Final Dataset}
In the final dataset there is a total of 68,512 methods, that span over 1,729,276 lines of codes. This makes the average length of a method in our dataset 25.24 lines.
For each method we have a buggy version and a fixed one, in particular the table \ref{tab:finaldataset_info} describe our dataset composition.\\

\begin{table}[H]
  \centering
  \begin{tabular}{|l|l|}
    \hline Repositories  & 23,051 \\
    \hline Commits       & 25,544 \\
    \hline Buggy methods & 34,256 \\
    \hline Fixed methods & 34,256 \\
    \hline
  \end{tabular}
  \caption{The dataset composition}
  \label{tab:finaldataset_info}
\end{table}


Let's take a look at a real example of a method from our dataset that the model has been tested on.

\begin{minipage}{\linewidth}
  \begin{lstlisting}[language=Java, caption=A bugged method from our dataset, label=lst:real_method_example_before]
/**
* the detail menu controller builder. 
* loads detail menu after controller is built.
* @param room the room to be shown. If null, display doc.
* @param doc the doctor to be shown. If null, display room
* doc and room will never be both null.
*/
private void DMCBuilder (Point room, Physician doc) {
 Stage primaryStage = (Stage) anchorPane.getScene().getWindow();
 try {
   FXMLLoader loader = new FXMLLoader(
     getClass().getResource("/DetailMenu.fxml")
    );
   DetailMenuController DC = new DetailMenuController(room, doc);
   loader.setController(DC);
   Pane mainPane = loader.load();
#  primaryStage.setScene(new Scene(mainPane, x_res, y_res));
 } catch (Exception e) {
   System.out.println("Cannot load main menu");
   e.printStackTrace();
 }
}
\end{lstlisting}
\end{minipage}

The line marked with a \# contains a bug, in fact the code after the commit contains:

\begin{minipage}{\linewidth}
  \begin{lstlisting}[language=Java, caption=A fixed method from our dataset, label=lst:real_method_example_after]
/**
* the detail menu controller builder. 
* loads detail menu after controller is built.
* @param room the room to be shown. If null, display doc.
* @param doc the doctor to be shown. If null, display room
* doc and room will never be both null.
*/
private void DMCBuilder (Point room, Physician doc) {
 Stage primaryStage = (Stage) anchorPane.getScene().getWindow();
 try {
   FXMLLoader loader = new FXMLLoader(
     getClass().getResource("/DetailMenu.fxml")
     );
   DetailMenuController DC = new DetailMenuController(room, doc);
   loader.setController(DC);
   Pane mainPane = loader.load();
#  primaryStage.getScene().setRoot(mainPane);
 } catch (Exception e) {
   System.out.println("Cannot load main menu");
   e.printStackTrace();
 }
}
\end{lstlisting}
\end{minipage}

Here you can see the same line fixed, where it used to create a new \texttt{Scene} using the referenced \texttt{mainPane}, it now simply passes the appropriate reference.
This method, before beign fed to the classifier, is transformed like this:

\begin{minipage}{\linewidth}
  \begin{lstlisting}[language=Java, caption=The transformed version of the bugged method, label=lst:transformed_method_example_before]
private void method(Type6 parameter0, Type7 parameter1) {
    Type0 variable1 = (Type0) variable0.methodCall0().methodCall1();
    try {
        Type2 variable2 = new Type2(methodCall2().methodCall3("string"));
        Type3 variable4 = new Type3(variable3, doc);
        variable2.methodCall4(variable4);
        Type4 variable5 = variable2.methodCall5();
#       variable1.methodCall6(new Type5(variable5, variable6, variable7));
    } catch (Type1 e) {
        System.out.println("string");
        e.printStackTrace();
    }
}
\end{lstlisting}
\end{minipage}

After the fix it will be transformed into:

\begin{minipage}{\linewidth}
  \begin{lstlisting}[language=Java, caption=The transformed version of the fixed method, label=lst:transformed_method_example_after]
private void method(Type5 parameter0, Type6 parameter1) {
    Type0 variable1 = (Type0) variable0.methodCall0().methodCall1();
    try {
        Type2 variable2 = new Type2(methodCall2().methodCall3("string"));
        Type3 variable4 = new Type3(variable3, doc);
        variable2.methodCall4(variable4);
        Type4 variable5 = variable2.methodCall5();
#       variable1.methodCall0().methodCall6(variable5);
    } catch (Type1 e) {
        System.out.println("string");
        e.printStackTrace();
    }
}
\end{lstlisting}
\end{minipage}
When tested the first input on our model the selected line was successfuly classified as buggy.

\section{The Deep Learning Model}
With the dataset that we constructed, our model need to perform a basic text classification, the layers are in fact similar to how a simple movie-review classificator would be.
However we needed to help the model to better recognize which line are we asking to be evaluated as buggy and which lines are the ``context'' around the bug.
For this reason there are 3 input layers in our model, given a single method with a line marked as a bug we split it into 3: the lines of code before the bug, the bugged line, the lines of code after the bug.

Each of this input is then embedded, and fed into a RNN. The result of each RNN is then combined and fed into a dense layer. The image \ref{fig:model} show the structure of the model.
\begin{figure}[H]
  \includegraphics[width=\linewidth]{images/model_plot.png}
  \caption{The plot of the model}
  \label{fig:model}
\end{figure}

This is just the general structure, however, we explored 1080 different combination of hyperparameters.
In particular for each of these parameter we tested:

\begin{itemize}
  \item Embedding dimension: 50, \textbf{80}, 100
  \item Optimizer: Nadam, \textbf{Adam}
  \item Hidden Units of the RNN: \textbf{16}, 32, 64
  \item Batch size: \textbf{32}, 64, 128
  \item Epochs: 20, 40, 60, 80, \textbf{100}
  \item Extra RNN layers: 0, \textbf{1}, 2, 3
\end{itemize}

The elements in bold rappresent the ones that we choose in our final model.\\
During the training we split our dataset into 80\% training, 10\% validation and 10\% test.
The table \ref{tab:hyperparameters_results} show the best 10 results of the hyperparameters search, sorted by accuracy:
\begin{table}[H]
  \centering
  \begin{tabular}{|l|l|l|l|l|l|l|l|l|l|}
    \hline
    \scriptsize \textbf{Val loss}   &
    \scriptsize \textbf{Val acc}    &
    \scriptsize \textbf{Loss}       &
    \scriptsize \textbf{Acc}        &
    \scriptsize \textbf{Batch Size} &
    \scriptsize \textbf{Embdedding} &
    \scriptsize \textbf{Epochs}     &
    \scriptsize \textbf{Hidden }    &
    \scriptsize \textbf{Optimizer}  &
    \scriptsize \textbf{Extra RNN}                                                       \\
    \hline 0.75                     & 0.53 & 0.61 & 0.65 & 32 & 80 & 100 & 16 & Adam & 1 \\
    \hline 0.77                     & 0.53 & 0.6  & 0.63 & 32 & 50 & 60  & 32 & Adam & 1 \\
    \hline 0.72                     & 0.54 & 0.61 & 0.63 & 32 & 50 & 100 & 16 & Adam & 1 \\
    \hline 0.76                     & 0.53 & 0.61 & 0.63 & 32 & 50 & 60  & 32 & Adam & 2 \\
    \hline 0.72                     & 0.52 & 0.62 & 0.63 & 32 & 50 & 100 & 16 & Adam & 2 \\
    \hline 0.75                     & 0.53 & 0.62 & 0.63 & 32 & 80 & 80  & 32 & Adam & 1 \\
    \hline 0.73                     & 0.53 & 0.61 & 0.63 & 32 & 50 & 80  & 16 & Adam & 2 \\
    \hline 0.73                     & 0.52 & 0.61 & 0.63 & 32 & 50 & 60  & 16 & Adam & 2 \\
    \hline 0.72                     & 0.53 & 0.6  & 0.63 & 32 & 50 & 80  & 16 & Adam & 1 \\
    \hline 0.74                     & 0.54 & 0.62 & 0.62 & 32 & 80 & 80  & 16 & Adam & 1 \\
    \hline
  \end{tabular}
  \caption{Hyperparameters training results}
  \label{tab:hyperparameters_results}
\end{table}


We can see that from the top results the optimal batch size seems to be 32 using the Adam optimizer, where the other parameters instead can appear in different combinations

